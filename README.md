# mountiso
Part of MultiOS-USB.  
https://gitlab.com/MultiOS-USB  
https://github.com/Mexit/MultiOS-USB  

A helper program that mounts the ISO image in the original releases of Windows 10 and 11.